import os

import pytest
import boto3

from bitbucket_pipes_toolkit.test import PipeTestCase


AWS_DEFAULT_REGION = 'us-east-1'
AWS_CFN_TEMPLATE_NAME_JSON = 'cfn_template.json'
AWS_CFN_TEST_STACK_NAME = f'cfn-deploy-test-us-east-{os.getenv("BITBUCKET_BUILD_NUMBER")}'

S3_BUCKET = os.getenv('S3_BUCKET')

# Inspired by https://bitbucket.org/awslabs/aws-cloudformation-bitbucket-pipelines-python"
cfn_template = '''
{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "AWS CloudFormation Sample Template DynamoDB_Table: This template demonstrates the creation of a DynamoDB table.  **WARNING** This template creates an Amazon DynamoDB table. You will be billed for the AWS resources used if you create a stack from this template. Inspired by https://bitbucket.org/awslabs/aws-cloudformation-bitbucket-pipelines-python",
    "Parameters": {
        "HashKeyElementName": {
            "Description": "HashType PrimaryKey Name",
            "Type": "String",
            "AllowedPattern": "[a-zA-Z0-9]*",
            "Default": "Name",
            "MinLength": "1",
            "MaxLength": "2048",
            "ConstraintDescription": "must contain only alphanumberic characters"
        },
        "HashKeyElementType": {
            "Description": "HashType PrimaryKey Type",
            "Type": "String",
            "Default": "S",
            "AllowedPattern": "[S|N]",
            "MinLength": "1",
            "MaxLength": "1",
            "ConstraintDescription": "must be either S or N"
        },
        "ReadCapacityUnits": {
            "Description": "Provisioned read throughput",
            "Type": "Number",
            "Default": "2",
            "MinValue": "1",
            "MaxValue": "10000",
            "ConstraintDescription": "must be between 5 and 10000"
        },
        "WriteCapacityUnits": {
            "Description": "Provisioned write throughput",
            "Type": "Number",
            "Default": "1",
            "MinValue": "1",
            "MaxValue": "10000",
            "ConstraintDescription": "must be between 5 and 10000"
        }
    },
    "Resources": {
        "myDynamoDBTable": {
            "Type": "AWS::DynamoDB::Table",
            "Properties": {
                "AttributeDefinitions": [
                    {
                        "AttributeName": {
                            "Ref": "HashKeyElementName"
                        },
                        "AttributeType": {
                            "Ref": "HashKeyElementType"
                        }
                    }
                ],
                "KeySchema": [
                    {
                        "AttributeName": {
                            "Ref": "HashKeyElementName"
                        },
                        "KeyType": "HASH"
                    }
                ],
                "ProvisionedThroughput": {
                    "ReadCapacityUnits": {
                        "Ref": "ReadCapacityUnits"
                    },
                    "WriteCapacityUnits": {
                        "Ref": "WriteCapacityUnits"
                    }
                }
            },
            "Metadata": {
                "AWS::CloudFormation::Designer": {
                    "id": "64b55d13-c3ad-4304-a759-fe01cd53fb46"
                }
            }
        }
    },
    "Outputs": {
        "TableName": {
            "Value": {
                "Ref": "myDynamoDBTable"
            },
            "Description": "Table name of the newly created DynamoDB table"
        }
    },
    "Metadata": {
        "AWS::CloudFormation::Designer": {
            "64b55d13-c3ad-4304-a759-fe01cd53fb46": {
                "size": {
                    "width": 60,
                    "height": 60
                },
                "position": {
                    "x": 190,
                    "y": 120
                },
                "z": 1,
                "embeds": []
            }
        }
    }
}
'''


def get_waiter_stack(stack_name, waiter_name):
    # waiter stack
    client = boto3.client(
        'cloudformation',
        region_name=os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION))

    waiter = client.get_waiter(waiter_name)

    # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudformation.html#CloudFormation.Waiter.StackUpdateComplete
    # default time to wait 30*120 sec
    # will wait fo 30*10 sec
    waiter_params = {
        'StackName': stack_name,
        'WaiterConfig': {
            'Delay': 60,
            'MaxAttempts': 10
        }
    }

    return waiter.wait(**waiter_params)


def delete_stack(stack_name, waiter_name):
    # delete stack
    client = boto3.client(
        'cloudformation',
        region_name=os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION))

    waiter = client.get_waiter(waiter_name)

    # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudformation.html#CloudFormation.Waiter.StackUpdateComplete
    # default time to wait 30*120 sec
    # will wait fo 30*10 sec
    waiter_params = {
        'StackName': stack_name,
        'WaiterConfig': {
            'Delay': 60,
            'MaxAttempts': 10
        }
    }

    if waiter.wait(**waiter_params) is None:
        client.delete_stack(StackName=stack_name)


class CloudFormationTestCase(PipeTestCase):
    template_s3 = f'https://s3.amazonaws.com/{S3_BUCKET}/dynamodb-cfn-deploy-pipes.template'
    template_s3_with_params = f'https://s3.amazonaws.com/{S3_BUCKET}/cfn_template_with_params.json'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        with open(AWS_CFN_TEMPLATE_NAME_JSON, 'w') as t:
            t.write(cfn_template)

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        os.remove(AWS_CFN_TEMPLATE_NAME_JSON)
        delete_stack(AWS_CFN_TEST_STACK_NAME, waiter_name='stack_update_complete')

    @pytest.mark.run(order=1)
    def test_create_successful_from_aws_s3(self):
        result = self.run_container(
            environment={
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
                'STACK_NAME': AWS_CFN_TEST_STACK_NAME,
                'TEMPLATE': self.template_s3,
                'WAIT': True,
                'WAIT_INTERVAL': 60,
            }
        )

        self.assertRegex(
            result, rf'✔ Successfully created the {AWS_CFN_TEST_STACK_NAME} stack')
        # test AWS CloudFormation text
        self.assertRegex(result, r'Stack')
        self.assertRegex(result, r'does not exist')

    @pytest.mark.run(order=2)
    def test_fail_if_no_template_provided(self):
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'STACK_NAME': AWS_CFN_TEST_STACK_NAME,
        })

        self.assertRegex(
            result, r'✖ Validation errors')

    @pytest.mark.run(order=3)
    def test_update_successful_if_no_updates(self):
        result = self.run_container(
            environment={
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
                'STACK_NAME': AWS_CFN_TEST_STACK_NAME,
                'TEMPLATE': self.template_s3,
            }
        )

        self.assertRegex(
            result, rf'✔ Successfully updated the {AWS_CFN_TEST_STACK_NAME} stack')
        # test AWS CloudFormation text
        self.assertRegex(result, r'No updates are to be performed')

    @pytest.mark.run(order=4)
    def test_update_successful_from_local_file_with_artifact(self):
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'STACK_NAME': AWS_CFN_TEST_STACK_NAME,
            'TEMPLATE': AWS_CFN_TEMPLATE_NAME_JSON,
            'WAIT': True,
            'WAIT_INTERVAL': 30,
            'OUTPUTS_FILENAME': 'outputs'
        })

        self.assertRegex(
            result, rf'✔ Successfully updated the {AWS_CFN_TEST_STACK_NAME} stack')

        self.assertRegex(result, 'UPDATE_COMPLETE')
        self.assertRegex(result, 'AWS::CloudFormation::Stack')
        self.assertTrue(os.path.exists('outputs.json'))

    @pytest.mark.run(order=5)
    def test_update_successful_with_extra_params(self):
        stack_parameters = '''
        [
            {
                "ParameterKey": "DBUser",
                "ParameterValue": "user"
            },
            {
                "ParameterKey": "DBPassword",
                "ParameterValue": "TestSuperSecrettt123"
            }
        ]
        '''

        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'STACK_NAME': AWS_CFN_TEST_STACK_NAME,
            'TEMPLATE': self.template_s3_with_params,
            'STACK_PARAMETERS': stack_parameters,
            'WAIT': True,
            'WAIT_INTERVAL': 60,
        })

        self.assertRegex(
            result, rf'✔ Successfully updated the {AWS_CFN_TEST_STACK_NAME} stack')

    @pytest.mark.run(order=6)
    def test_update_successful_with_tags(self):
        tags = '''
        [
            {
                "Key": "aTagKey",
                "Value": "aTagValue"
            },
            {
                "Key": "anotherTagKey",
                "Value": "anotherTagValue"
            }
        ]
        '''

        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'STACK_NAME': AWS_CFN_TEST_STACK_NAME,
            'TEMPLATE': AWS_CFN_TEMPLATE_NAME_JSON,
            'TAGS': tags,
            'WAIT': True,
            'WAIT_INTERVAL': 60,
            'BITBUCKET_REPO_FULL_NAME': 'mytestaccount/mytestrepo',
            'BITBUCKET_BRANCH': 'master',
            'BITBUCKET_COMMIT': '7f777ed95a19224294949e1b4ce56bbffcb1fe9f',
            'BITBUCKET_BUILD_NUMBER': 2
        })

        self.assertRegex(
            result, rf'✔ Successfully updated the {AWS_CFN_TEST_STACK_NAME} stack')

        client = boto3.client(
            'cloudformation',
            region_name=os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION))
        describeStackResult = client.describe_stacks(
            StackName=AWS_CFN_TEST_STACK_NAME)

        self.assertIn({
            "Key": "aTagKey",
            "Value": "aTagValue"
        }, describeStackResult['Stacks'][0]['Tags'])

        self.assertIn({
            "Key": "anotherTagKey",
            "Value": "anotherTagValue"
        }, describeStackResult['Stacks'][0]['Tags'])

        self.assertIn({
            "Key": "BITBUCKET_REPO_URL",
            "Value": "https://bitbucket.org/mytestaccount/mytestrepo"
        }, describeStackResult['Stacks'][0]['Tags'])

        self.assertIn({
            "Key": "BITBUCKET_BRANCH",
            "Value": 'master'
        }, describeStackResult['Stacks'][0]['Tags'])

        self.assertIn({
            "Key": "BITBUCKET_COMMIT",
            "Value": '7f777ed95a19224294949e1b4ce56bbffcb1fe9f'
        }, describeStackResult['Stacks'][0]['Tags'])

        self.assertIn({
            "Key": "BITBUCKET_BUILD_NUMBER",
            "Value": '2'
        }, describeStackResult['Stacks'][0]['Tags'])

    @pytest.mark.run(order=7)
    def test_oidc_update_stack_successful(self):
        result = self.run_container(environment={
            'AWS_OIDC_ROLE_ARN': os.getenv('AWS_OIDC_ROLE_ARN'),
            'BITBUCKET_STEP_OIDC_TOKEN': os.getenv('BITBUCKET_STEP_OIDC_TOKEN'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'STACK_NAME': AWS_CFN_TEST_STACK_NAME,
            'TEMPLATE': self.template_s3,
            'WAIT': True,
            'WAIT_INTERVAL': 120,
        })
        self.assertRegex(
            result, rf'✔ Successfully updated the {AWS_CFN_TEST_STACK_NAME} stack')
        self.assertIn('Authenticating with a OpenID Connect (OIDC) Web Identity Provider', result)


class CloudFormationFailTestCase(PipeTestCase):
    template = f'fail-{AWS_CFN_TEMPLATE_NAME_JSON}'
    stack_name = f'fail-{AWS_CFN_TEST_STACK_NAME}'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        with open(cls.template, 'w') as t:
            t.write(cfn_template)

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        os.remove(cls.template)
        # delete stack
        client = boto3.client(
            'cloudformation',
            region_name=os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION))
        client.delete_stack(StackName=cls.stack_name)

    @pytest.mark.skip(reason="Simplify work with negative-test-infra-user.")
    def test_update_fail_resource(self):
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY_FAIL'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID_FAIL'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'STACK_NAME': self.stack_name,
            'TEMPLATE': self.template,
            'WAIT': True,
        })

        self.assertRegex(
            result, r'✖ Failed to update the stack')


class SamTestCase(PipeTestCase):
    """
        AWS SAM (AWS Serverless Application Model)
        AWS SAM is an extension of AWS CloudFormation
    """
    stack_name = f'sam-{AWS_CFN_TEST_STACK_NAME}'

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        delete_stack(cls.stack_name, waiter_name='stack_create_complete')

    def test_create_successful(self):

        result = self.run_container(
            environment={
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
                'STACK_NAME': self.stack_name,
                # must be AWS S3 in variables
                'TEMPLATE': f'https://{S3_BUCKET}.s3.amazonaws.com/sam-deploy-pipe-packaged.yaml',
                'WAIT': True,
                'WAIT_INTERVAL': 60,
                'CAPABILITIES_COUNT': 2,
                'CAPABILITIES_0': 'CAPABILITY_IAM',
                'CAPABILITIES_1': 'CAPABILITY_AUTO_EXPAND',
            }
        )

        self.assertRegex(
            result, rf'✔ Successfully created the {self.stack_name} stack')


class CloudFormationTimeoutTestCase(PipeTestCase):
    template = f'https://{S3_BUCKET}.s3.amazonaws.com/cfn_ec2_template_default.json'
    stack_name = f'{AWS_CFN_TEST_STACK_NAME}-timeout'

    @classmethod
    def tearDownClass(cls):
        delete_stack(cls.stack_name, waiter_name='stack_rollback_complete')

    def test_cancel_update_on_timeout_happening(self):
        self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'STACK_NAME': self.stack_name,
            'TEMPLATE': self.template,
        })

        get_waiter_stack(self.stack_name, waiter_name='stack_create_complete')

        template = f'https://{S3_BUCKET}.s3.amazonaws.com/cfn_ec2_template_update.json'
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'STACK_NAME': self.stack_name,
            'TEMPLATE': template,
            'WAIT': True,
            'WAIT_INTERVAL': 0.2,
            'CANCEL_UPDATE_ON_TIMEOUT': True,
        })

        assert "Max attempts exceeded during wait for stack" in result
        self.assertRegex(
            result, rf'✔ Successfully canceled update the {self.stack_name} stack')
