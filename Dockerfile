FROM python:3.10-slim

COPY requirements.txt /
WORKDIR /
RUN pip install --no-cache-dir -r requirements.txt

COPY LICENSE.txt README.md pipe.yml /
COPY pipe /

ENTRYPOINT ["python3", "/main.py"]
