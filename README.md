# Bitbucket Pipelines Pipe: AWS CloudFormation deploy

Deploy your configuration as code using [AWS CloudFormation](https://aws.amazon.com/cloudformation/).

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:
    
```yaml
- pipe: atlassian/aws-cloudformation-deploy:0.20.1
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    AWS_OIDC_ROLE_ARN: "<string>" # Optional by default. Required for OpenID Connect (OIDC) authentication.
    STACK_NAME: '<string>'  # Optional
    TEMPLATE: '<string>'
    # STACK_PARAMETERS: '<json>' # Optional.
    # CAPABILITIES: '<array>' # Optional.
    # WAIT: '<boolean>' # Optional.
    # WAIT_INTERVAL: '<integer>' # Optional.
    # CANCEL_UPDATE_ON_TIMEOUT: '<boolean>' # Optional.
    # TAGS: '<json>' # Optional.
    # WITH_DEFAULT_TAGS: '<boolean>' # Optional.
    # ROLE_ARN: '<string>' # Optional.
    # EXTRA_PARAMETERS: '<string>' # Optional.
    # OUTPUTS_FILENAME: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

### Basic usage

| Variable                   | Usage                                                                                                                                                                                                                                                                                                                                                                   |
|----------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| AWS_ACCESS_KEY_ID (**)     | AWS access key.                                                                                                                                                                                                                                                                                                                                                         |
| AWS_SECRET_ACCESS_KEY (**) | AWS secret key.                                                                                                                                                                                                                                                                                                                                                         |
| AWS_DEFAULT_REGION (**)    | The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints][Regions and Endpoints] in the _Amazon Web Services General Reference_.                                                                                                                                                 |
| AWS_OIDC_ROLE_ARN          | The ARN of the role used for web identity federation or OIDC. See **Authentication**.                                                                                                                                                                                                                                                                                   |
| STACK_NAME                 | The name of the AWS CloudFormation stack. If the stack does not exist, it will be created. Default: `${BITBUCKET_REPO_OWNER}-${BITBUCKET_REPO_SLUG}-${BITBUCKET_BUILD_NUMBER}`                                                                                                                                                                                          |
| TEMPLATE (*)               | Location of the file containing a template body in your repository, or a URL that points to the template hosted in an Amazon S3 bucket. Valid template body formats are: `json` or `yaml`. See **Prerequisites**.                                                                                                                                                       |
| STACK_PARAMETERS           | JSON document containing variables to pass to a pipeline you want to trigger. The value should be a list of object with `ParameterKey`, `ParameterValue` fields for each of your variables. The **Examples** section below contains an example for passing parameters to the stack.                                                                                     |
| CAPABILITIES               | Array of [capabilities][capabilities]. Allowed values: [`CAPABILITY_IAM, CAPABILITY_NAMED_IAM, CAPABILITY_AUTO_EXPAND`].                                                                                                                                                                                                                                                |
| WAIT                       | Wait for deployment to complete. Default: `false`. If false, it will finish when the cloudformation deploy is triggered and will not wait until the resources are successfully created.                                                                                                                                                                                 |
| WAIT_INTERVAL              | Time to wait between polling for deployment to complete (in seconds). Default: `30`.                                                                                                                                                                                                                                                                                    |
| CANCEL_UPDATE_ON_TIMEOUT   | Cancels an update on the specified stack when timeout is reached (WAIT_INTERVAL * 120 attempts). Default: `false`. If the call completes successfully, the stack rolls back the update and reverts to the previous stack configuration.                                                                                                                                 |
| TAGS                       | JSON document containing tags to pass to a pipeline you want to trigger. The value should be a list of object with `Key`, `Value` fields for each of your tags. The **Examples** section below contains an example for passing tags to the stack.                                                                                                                       |
| WITH_DEFAULT_TAGS          | Whether or not to add the default tags: `BITBUCKET_REPO_URL` (with value `https://bitbucket.org/$BITBUCKET_REPO_FULL_NAME`) and `BITBUCKET_DEPLOYMENT_ENVIRONMENT` (with value `$BITBUCKET_DEPLOYMENT_ENVIRONMENT`, if applicable). Default: `true`. Tags specified with `TAGS` override the default tags.                                                              |
| ROLE_ARN                   | The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that AWS CloudFormation assumes to update the stack. If a value aren't specified, AWS CloudFormation uses the role that was previously associated with the stack. If no role is available, AWS CloudFormation uses a temporary session that is generated from your user credentials. |
| EXTRA_PARAMETERS           | JSON document containing other supported stack's parameters. The value should be a dictionary(mapping) {"key": "value"}. The **Examples** section below contains an example for passing parameters to the stack.                                                                                                                                                        |
| OUTPUTS_FILENAME           | Works only with `WAIT: True`. File name to store cloudformation outputs in json format (extension will be added automatically). If provided, then cloudformation output file will be created in BITBUCKET_CLONE_DIR directory.                                                                                                                                          |
| DEBUG                      | Turn on extra debug information. Default: `false`.                                                                                                                                                                                                                                                                                                                      |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


## Details

This pipe creates or updates a CloudFormation stack associated with the projects AWS cloud infrastructure.

AWS CloudFormation allows you to create and manage AWS infrastructure deployments predictably and repeatedly. You can use AWS CloudFormation to leverage AWS products, such as Amazon Elastic Compute Cloud, S3, etc. to build highly-reliable, highly scalable, cost-effective applications without creating or configuring the underlying AWS infrastructure.


## Authentication

Supported options:

1. Environment variables: AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY. Default option.

2. Assume role provider with OpenID Connect (OIDC). More details in the Bitbucket Pipelines Using OpenID Connect guide [Integrating aws bitbucket pipeline with oidc][aws-oidc]. Make sure that you setup OIDC before:
    * configure Bitbucket Pipelines as a Web Identity Provider in AWS
    * attach to provider your AWS role with required policies in AWS
    * setup a build step with `oidc: true` in your Bitbucket Pipelines
    * pass AWS_OIDC_ROLE_ARN (*) variable that represents role having appropriate permissions to execute actions on Cloudformation resources


#### Tags added by default

By default, the pipe will use the following tags in order to track which pipeline created the AWS resources and be able to link them to the Cloudformation templates:

| Tag                      | Description                                                                                                                                                              |
|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `BITBUCKET_BRANCH`       | The source branch. Example: `master`                                                                                                                                     |
| `BITBUCKET_COMMIT`       | The commit hash of a commit that kicked off the build. Example: `7f777ed95a19224294949e1b4ce56bbffcb1fe9f`                                                               |
| `BITBUCKET_BUILD_NUMBER` | The unique identifier for a build. It increments with each build and can be used to know which pipeline created the AWS resources. Example: `2`                          |
| `BITBUCKET_REPO_URL`     | The repository URL. It can be used to quickly link to the repository that contains the Cloudformation templates. Example: `https://bitbucket.org/your-company/your-repo` |
 

## Prerequisites
* An IAM user or Web Identity Provider (OIDC) role is configured with sufficient permissions to perform a deployment of your application using CloudFormation.
* You have configured the AWS CloudFormation stack and environment.
* CloudFormation templates greater than 51200 bytes must be deployed from an AWS S3 bucket by providing AWS S3 URL. For further details please see [AWS CloudFormation limits][AWS CloudFormation limits].


## Examples 

### Basic example:

Deploy a new version of your CloudFormation stack, using a template file located in an Amazon S3 bucket.
    
```yaml
script:
  - pipe: atlassian/aws-cloudformation-deploy:0.20.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/cfn-deploy-pipe/cfn-template.json'
```

Deploy a new version of your CloudFormation stack, using a template file located in an Amazon S3 bucket. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.
    
```yaml
script:
  - pipe: atlassian/aws-cloudformation-deploy:0.20.1
    variables:
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/cfn-deploy-pipe/cfn-template.json'
```

Deploy a new version of your CloudFormation stack, using a template file located in your repository.
    
```yaml
script:
  - pipe: atlassian/aws-cloudformation-deploy:0.20.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'stack_template.json'
```

### Advanced example:
Deploy a new version of your CloudFormation stack with OpenID Connect (OIDC) alternative authentication without required `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`.
Parameter `oidc: true` in the step configuration and variable `AWS_OIDC_ROLE_ARN` are required:

```yaml

- step:
    oidc: true
    script:
      - pipe: atlassian/aws-cloudformation-deploy:0.20.1
        variables:
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          AWS_OIDC_ROLE_ARN: 'arn:aws:iam::123456789012:role/role_name'
          STACK_NAME: 'my-stack-name'
          TEMPLATE: 'stack_template.json'
```

Upload a new version of CloudFormation stack template to the AWS S3 Bucket with [AWS S3 deploy pipe](https://bitbucket.org/atlassian/aws-s3-deploy) and then deploy a new version of your CloudFormation stack, using a template file located in an Amazon S3 bucket.

```yaml
script:
  - pipe: atlassian/aws-s3-deploy:0.2.4
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'cfn-deploy-pipe'
      LOCAL_PATH: 'directory-with-cfn-template-big-file.json'
  - pipe: atlassian/aws-cloudformation-deploy:0.20.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/cfn-deploy-pipe/cfn-template-big-file.json'
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
```


Deploy a new version of your CloudFormation stack and wait until the deployment has completed. Store stack outputs in `outputs.json` file.

```yaml
script:
  - pipe: atlassian/aws-cloudformation-deploy:0.20.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/cfn-deploy-pipe/cfn-template.json'
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
      WAIT: 'true'
      WAIT_INTERVAL: 60
      OUTPUTS_FILENAME: 'outputs'
artifacts:
  - outputs.json
```

Deploy a new version of your CloudFormation stack with additional parameters for the stack.

```yaml
script:
  - pipe: atlassian/aws-cloudformation-deploy:0.20.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/cfn-deploy-pipe/cfn-template.json'
      STACK_PARAMETERS: >
          [{
            "ParameterKey": "KeyName",
            "ParameterValue": "mykey"
          },
          {
            "ParameterKey": "DBUser",
            "ParameterValue": "mydbuser"
          },
          {
            "ParameterKey": "DBPassword",
            "ParameterValue": $DB_PASSWORD
          }]
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
```

Deploy a new version of your CloudFormation stack with tags.

```yaml
script:
  - pipe: atlassian/aws-cloudformation-deploy:0.20.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/cfn-deploy-pipe/cfn-template.json'
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
      TAGS: >
          [{
            "Key": "Environment",
            "Value": "TEST"
          },
          {
            "Key": "Application",
            "Value": "myApp"
          }]
```

Deploy a new version of your CloudFormation stack with an explicit role.

```yaml
script:
  - pipe: atlassian/aws-cloudformation-deploy:0.20.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/cfn-deploy-pipe/cfn-template.json'
      ROLE_ARN: 'arn:aws:iam::123456789012:role/your-awesome-role-name'
```

Deploy a new version of your CloudFormation stack with extra parameters for the stack.

```yaml
script:
  - pipe: atlassian/aws-cloudformation-deploy:0.20.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/cfn-deploy-pipe/cfn-template.json'
      EXTRA_PARAMETERS: >
        {
          "ResourceTypes": ["AWS::EC2::Instance"],
          "NotificationARNs": ["arn:aws:sns:us-east-1:111122223333:my-topic"],
          "ClientRequestToken": "Console-CreateStack-7f59c3cf-00d2-40c7-b2ff-e75db0987002"
        }
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,aws,cloudformation
[capabilities]: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-iam-template.html#using-iam-capabilities
[aws-oidc]: https://support.atlassian.com/bitbucket-cloud/docs/deploy-on-aws-using-bitbucket-pipelines-openid-connect
[AWS CloudFormation limits]: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cloudformation-limits.html
[Regions and Endpoints]: https://docs.aws.amazon.com/general/latest/gr/rande.html
